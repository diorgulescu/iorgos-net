## What I'm doing NOW

+ Providing loving daycare to my second son (Radu), as the final months of parental leave approach. This leave will end in September 2022.
+ Actively undertaking the ["Python 3 Programming Professional Specialization"](https://www.coursera.org/specializations/python-3-programming) offered by the University of Michigan
+ Reviving & refactoring the [DAMF (Device Automation & Management Framework) project](https://github.com/diorgulescu/DAMF) in a way that employs the use of microservices and Docker. Of course, further feature additions and a working PoC (Proof of Concept) are in the works (I plan on using three boards: a TI Launchpad MSP430,  one RaspberryPi Zero W and, maybe, an Arduino running Xinu :) )